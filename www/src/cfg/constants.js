export default {
    GRAPHQL_URI : `http://localhost:${ 8000 }/graphql`, // backend, singular gql endpoint, fe port defined in webpack config
    AUTH_TOKEN_KEY : "auth-token"
};