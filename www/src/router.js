import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link,
    Switch // Enables: first-match return (if multiple matches), transition-animation, 404
} from "react-router-dom";
import {withRouter} from "react-router";

import views from "./views";

const Header = withRouter(() => (
    <div>
        <Link to="/login">Login</Link>
        <Link to="/register">Register</Link>
        <Link to="/exchange">Exchange</Link>
    </div>
));

const IS_AUTHED = false;
const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            IS_AUTHED ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: {from: props.location}
                    }}
                />
            )
        }
    />
);

module.exports = props => {
    var {
        location // from React Router
    } = props;

    return (
        <Router>
            <div>
                <Header />
                <Switch>
                    <Route exact path="/login" component={views.Login} />
                    <Route exact path="/register" component={views.Register} />

                    <PrivateRoute exact path="/exchange" component={views.Exchange} />
                    <PrivateRoute exact path="/" component={views.Exchange} />
                </Switch>
            </div>
        </Router>
    );
};
