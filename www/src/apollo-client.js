import _                  from "lodash";
import { createHttpLink } from "apollo-link-http";
import { ApolloClient }   from "apollo-client";
import { InMemoryCache }  from "apollo-cache-inmemory";
import { ApolloLink }     from "apollo-link";
import { onError }        from "apollo-link-error";
import { setContext }     from "apollo-link-context";

import cfg from "./cfg";

const httpLink = createHttpLink({
    uri : cfg.constants.GRAPHQL_URI
});

const authLink = setContext( ( _, { headers } ) => {
    const token = localStorage.getItem( cfg.constants.AUTH_TOKEN_KEY );
    return {
        headers : {
            ... headers,
            authorization : token ? `Bearer ${ token }` : null
        }
    };
} );

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if( graphQLErrors ) {
        _.map( graphQLErrors, ({ message, locations, path }) => {
            const locationsToLog = _.chain( locations )
                .map( x => `COLUMN-${ x.column }-LINE-${ x.line }` )
                .reduce( ( str1, str2 ) => `${ str1 } ${ str2 }`, "" )
                .value();
            
            console.log( "%c GraphQL Error", "background : red; color : white" );
            console.log( `Message : ${ message } \n Locations : ${ locationsToLog } \n Path : ${ path }` );
        } )
    }

    if ( networkError ) {
        console.log( "%c Network Error", "background : red; color : white" );
        console.log( networkError );
    }
});

const link = ApolloLink.from( [ errorLink, authLink, httpLink ] );

export default new ApolloClient({
    link,
    cache : new InMemoryCache()
});