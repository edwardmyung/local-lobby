// Private routes: https://tylermcginnis.com/react-router-protected-routes-authentication/
// https://stackoverflow.com/questions/38397653/redux-what-is-the-correct-place-to-save-cookie-after-login-request

import React    from "react";
import ReactDOM from "react-dom";

import { ApolloProvider } from "react-apollo";
import { Provider }       from "react-redux";
import Router             from "./router";
import { store }          from "./store";
import apolloClient       from "./apollo-client";

import "babel-polyfill"; // required for promises

document.addEventListener( "DOMContentLoaded", function() {
    ReactDOM.render( 
        <ApolloProvider client={ apolloClient }>
            <Provider store={ store }>
                <Router />
            </Provider>
        </ApolloProvider>, 
        document.getElementById( "root" )
    );
});