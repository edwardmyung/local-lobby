import React from "react";
import _    from "lodash";
import { connect } from "react-redux";

import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { Field, reduxForm, formValueSelector } from "redux-form";

const LOGIN_MUTATION = gql`
    mutation LoginMutation( $email : String!, $password : String! ){
        login( 
            email    : $email, 
            password : $password
        ) {
            token
        }
    }
`;

const renderField = ({ input, label, type, meta: { touched, error } }) => {
    const hasError = (touched && error) ? 'has-danger' : '';
    return (
        <div className={`form-group ${hasError}`}>
            <label>{label}</label>
            <div>
                <input {...input} placeholder={label} type={type} className="form-control" />
                {touched && error && <div className="form-control-feedback">{error}</div>}
            </div>
        </div>
    );
};

const renderErrors = (errors) => (
    <div className="alert alert-danger" role="alert">
        { _.map( errors, (error, index) => <span key={ index }>{ error.value }</span> ) }
    </div>
);
  
class SignInForm extends React.Component {
    constructor( props ) {
        super( props );
    }
    
    _confirm( data ) {
        console.log( data );
    }

    render() {
        const errors = this.props.errors <= 0 ? null : renderErrors( this.props.errors );
    
        return (
            <form onSubmit={ e => e.preventDefault() }>
                { errors }
                <Field name="email" type="email" component={ renderField } label="Email" />
                <Field name="password" type="password" component={ renderField } label="Password" />
                <Mutation 
                    mutation={ LOGIN_MUTATION }
                    variables={{ 
                        email : this.props.email,
                        password : this.props.password
                    }}
                    onCompleted={ data => this._confirm( data ) }>
                    {
                        ( mutation, { loading, error } ) => {
                            let errorMessage, loadingMessage;
                            
                            if( error )
                                errorMessage = <div>Login was unsuccessful, please try again</div>;
                            
                            if( loading )
                                loadingMessage = <div>Loading</div>;
                            return (
                                <div>
                                    { errorMessage }
                                    { loadingMessage }
                                    <button onClick={ mutation }>
                                        Login
                                    </button> 
                                </div>
                            );
                        }
                    }
                </Mutation>
            </form>
        );
    }
}

const FinalForm = reduxForm({
    form : "signInForm",
    validate
})( SignInForm );

const validate = ( values ) => {
    const errors = {};
    // ... do validations here 
    return errors;
}

const mapStateToProps = s => {
    const { email, password } = formValueSelector( "signInForm" )( s, "email", "password" );
    return { email, password };
};
const mapDispatchToProps = d => ({ x : 3 });
  
export default connect( mapStateToProps, mapDispatchToProps )( FinalForm );