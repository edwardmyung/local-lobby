import { default as Register } from "./register";
import { default as Login }    from "./login";
import { default as Exchange } from "./exchange";

export default {
    Register,
    Login,
    Exchange
};