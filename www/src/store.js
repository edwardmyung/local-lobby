import { 
    createStore, 
    applyMiddleware,
    combineReducers
} from "redux";
import { composeWithDevTools }    from "redux-devtools-extension";
import { logger }                 from "redux-logger";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
    form : formReducer
});

const middlewares = [
    logger
];

const initialState = {};

export const store = createStore(
    rootReducer,
    initialState,                                                               // state to hydrate with
    composeWithDevTools( applyMiddleware( ... middlewares ) )                   // 3rd party redux enhancers 
);