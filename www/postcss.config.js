// npm install -D these plugins, then use post-css-loader
// Benefits of postcss - no need for prefixes, use modern css, etc(See https://postcss.org/)

module.exports = {
    plugins : {
        "autoprefixer" : {},
        "cssnano" : {},
        "postcss-import" : {},
        "postcss-preset-env" : {}
    }
};