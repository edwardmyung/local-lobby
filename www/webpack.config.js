// TODO: add graphql-tag for graphql|gql files, as per
// https://www.apollographql.com/docs/react/recipes/webpack.html


/*
* Ensuring page loads when refreshed at a URL: https://tylermcginnis.com/react-router-cannot-get-url-refresh/
*/

const path = require( "path" );
const isDev = process.env.NODE_ENV !== "production";

const HtmlWebpackPlugin    = require( "html-webpack-plugin" );
const CleanWebpackPlugin   = require( "clean-webpack-plugin" );
const MiniCssExtractPlugin = require( "mini-css-extract-plugin" );

module.exports = {
    // sets default path (e.g. for entry)
    context: path.resolve( __dirname, "src" ),

    // app root index.js
    entry : "./index.js",

    // output destination of index.bundle.js and index.html
    output : {
        filename : "bundle.js",
        path : path.resolve( __dirname, "dist" ),
        publicPath: "/"
    },
    
    plugins : [
        // adds the bundled js into html and outputs html into output.path
        new HtmlWebpackPlugin({
            template : path.join( __dirname, "index.html" )
        }),

        // removes dist folder whenever it rebuilds. Now:
        // 'npm run build' => removes then re-adds dist/ to remove unused files - important in prod
        // 'npm run dev'   => running the app doesn't create dist, not efficient but good for rapid FE dev-cycle
        new CleanWebpackPlugin( [ "dist" ] ),

        // Ensures css files are actual files on dist, not just loaded by JS on DOMContentLoaded
        new MiniCssExtractPlugin({
            filename      : isDev ? "[name].css" : "[name].[hash].css",
            chunkFilename : isDev ? "[id].css" : "[id].[hash].css"
        })
    ],

    // config for webpack-dev-server (watches for file changes)
    devServer : {
        contentBase        : path.resolve( __dirname, "dist/assets" ),      // where to store our content (e.g. images)
        open               : true,                                          // auto open browser to show
        compress           : true,                                          // activate gzip
        stats              : "errors-only",
        port               : 5001,
        historyApiFallback : true,                                    
    },

    module: {
        // all loaders require a test rule for file-extension
        // if test condition is met, applies the loader
        rules: [
            
            // file-loader
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "./assets/"
                    }
                }
            },

            // scss loaders
            {
                test: /\.(sc|sa|c)ss$/,
                use: [ 
                    isDev ? "style-loader" : MiniCssExtractPlugin.loader,   // In prod we want requires to bring css into own files, we need isDev test to preserve Hot-Module-Replacement on dev
                    "css-loader",                                           // allows require of .css files
                    "sass-loader",                                          // allows require of .scss files
                    "postcss-loader"                                        // calls the postcss.config.js in path-level of webpack.config.js
                ] // order matters, but note last loader is run first :-)
            },

            // babel loaders
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader : "babel-loader",
                    options : {
                        presets : [ "env", "react" ],
                        plugins : [ "transform-object-rest-spread" ]
                    }
                }
            }
        ]
    }

};