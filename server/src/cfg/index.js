import { default as constants } from "./constants";
import * as middlewares from "./middlewares";

export default {
    constants,
    middlewares
};