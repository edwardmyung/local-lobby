/*
    * This file loads from the process, this is either from:
    *   
    *   1. dotenv : non-production environments
    *   2. host   : hosted server (e.g. AWS EC2 instance)
    *
    * Need to ensure the variables env is setup when this file is built
    * Therefore the dotenv require is here, not in app.js
*/

if ( process.env.NODE_ENV === "dev" )
    require( "dotenv" ).config({ path: "variables/dev.env" });

export default {
    PORT         : process.env.PORT || 8000,
    DB_URL       : `mongodb://${ process.env.DB_USERNAME }:${ process.env.DB_PASSWORD }@${ process.env.DB_URL_HOST }`,
    JWT_SECRET   : process.env.JWT_SECRET,
    GRAPHQL_PATH : "/graphql"
};