import util from "../util";

export const auth = async ( req, _res, next ) => {
    try {
        // parse string to prevent ( null -> false ) being cast to ( "null" -> true )
        const token = JSON.parse( req.headers.authorization );

        if ( token ) {
            const user = await util.auth.decodeToken( token ); // throws error if invalid token
            req.user = user;
        } else {
            req.user = null;
        }

        return next();
    } catch ( err ) {
        throw err;
    }
};