import mongoose from "mongoose";
import constants from "./constants";

mongoose.Promise = global.Promise;

mongoose.set( "debug", true );

try {
    mongoose.connect(
        constants.DB_URL,
        { useNewUrlParser: true }
    );
} catch ( err ) {
    mongoose.createConnection( constants.DB_URL );
}

mongoose.connection
    .once( "open", () => console.log( "Success: Connected to MongoDB" ) )
    .on( "error", err => {
        throw err;
    });
