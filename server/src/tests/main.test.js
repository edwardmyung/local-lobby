/*
 * TODO: 
 * 1. get sequential tests on jest working
 * 2. build house group tests
 * 3. get global headers working
 * 4. build tests for remaining models
 **/

import helpers from "../helpers";
import userResolvers from "../../graphql/resolvers/user";

test( "User registers", async () => {    
    let parent = {};
    let args = {
        fullName    : "Edward Myung",
        email       : "edward.myung@googlemail.com",
        password    : "Foobar123",
        mobile      : "+44075121233184026176",
        shortId     : "ABCDE",
        houseNumber : 2
    };
    let ctx = {};
    
    let { token } = await userResolvers.register( parent, args, ctx );

    expect( typeof token ).toEqual( "string" ); 
} );