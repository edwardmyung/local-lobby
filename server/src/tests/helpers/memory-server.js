import mongoose            from "mongoose";
import MongodbMemoryServer from "mongodb-memory-server";

const memoryServer = new MongodbMemoryServer();

const initialize = () => {
    memoryServer.getConnectionString().then( ( mongoUri ) => {
    
        // options for mongoose 4.11.3 and above
        const mongooseOpts = {
            autoReconnect: true,
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 1000,
            useNewUrlParser: true
        };
    
        mongoose.connect( mongoUri, mongooseOpts );
    
        mongoose.connection.on( "error", e => {
            if ( e.message.code === "ETIMEDOUT" ) {
                console.log( e );
                mongoose.connect( mongoUri, mongooseOpts );
            }
            console.log( e );
        });
    
        mongoose.connection.once( "open", () => {
            console.log( `MongoDB successfully connected to ${ mongoUri }` );
        });
    });    
};

const close = () => {
    mongoose.disconnect();
    memoryServer.stop();
};

(() => {
    beforeAll( () => {
        initialize();
    } );
    
    afterAll( () => {
        close();
    } );
})();