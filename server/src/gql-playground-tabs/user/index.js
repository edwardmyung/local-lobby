const { readFileSync } = require( "fs" );

export default {
    authenticateResidence : readFileSync( __dirname + "/authenticate-residence.graphql", "utf8" ),
    register              : readFileSync( __dirname + "/register.graphql", "utf8" ),
    login                 : readFileSync( __dirname + "/login.graphql", "utf8" ),
    recoverAccount        : readFileSync( __dirname + "/recover-account.graphql", "utf8" )
};