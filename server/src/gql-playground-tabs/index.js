import cfg          from "../cfg";

import user         from "./user";
import exchangeItem from "./exchange-item";

const ENDPOINT = cfg.constants.GRAPHQL_PATH;
const HEADERS  = {
    authorization : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Yjk1NTMyNTFkMzk2ZTM4NDFjZmQ2N2QiLCJpYXQiOjE1MzY1MTI4OTN9.Vju2-9JW0Bd6cvgHfNtP1whYi5hzB4YZ_ftf0rFCWYQ"
};

export default [
    {
        endpoint : ENDPOINT,
        query    : user.register,
        name     : "Register",
        headers  : HEADERS
    },

    {
        endpoint : ENDPOINT,
        query    : user.login,
        name     : "Login",
        headers  : HEADERS
    },

    {
        endpoint : ENDPOINT,
        query    : user.authenticateResidence,
        name     : "Auth Residence",
        headers  : HEADERS
    },

    {
        endpoint : ENDPOINT,
        query    : exchangeItem.createExchangeItem,
        name     : "Create item",
        headers  : HEADERS
    },
    
    {
        endpoint : ENDPOINT,
        query    : exchangeItem.deleteExchangeItem,
        name     : "Delete item",
        headers  : HEADERS
    },

    {
        endpoint : ENDPOINT,
        query    : exchangeItem.updateExchangeItem,
        name     : "Update Item",
        headers  : HEADERS
    }
];