const { readFileSync } = require( "fs" );

export default {
    createExchangeItem : readFileSync( __dirname + "/create-exchange-item.graphql", "utf8" ),
    deleteExchangeItem : readFileSync( __dirname + "/delete-exchange-item.graphql", "utf8" ),
    updateExchangeItem : readFileSync( __dirname + "/update-exchange-item.graphql", "utf8" )
};