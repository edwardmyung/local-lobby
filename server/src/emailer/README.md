# Summary

The exported function simplifies how we interface with mandrill...

Example usage:

```js
email.sendTemplate({
    templateName : "mandrill-template-slug",
    subject      : "Subject",
    senderKey    : "key-from-email-constant",
    recipients   : [
        {
            name : "Thomas",
            email : "thomas.smith@gmail.com",
            mergeVars : {
                firstName : "Thomas",
                lastName : "Smith"
            }
        }
    ]
});