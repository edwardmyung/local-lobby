import _             from "lodash" ;
import mandrill      from "mandrill-api/mandrill";
import baseCfg       from "./cfg";
import emailConstants from "./constants";

const mandrill_client = new mandrill.Mandrill( process.env.MANDRILL_API_KEY );

const generateSenderCfg = ( senderKey ) => {
    const sender = emailConstants.senders[ senderKey ];
    return {
        from_name   : sender.name,
        from_email  : sender.email,
        bcc_address : sender.email,
        headers : {
            "Reply-To" : sender.email
        },
    };
};

const generateRecipientCfg = ( recipients ) => {
    return {
        to : _.map( recipients, r => ({
            name  : r.name,
            email : r.email,
            type  : "to"
        }) )
    };
};

const generateMergeVarsCfg = ( recipients ) => {
    return {
        merge_vars : _.map( recipients, r => ({
            rcpt : r.email,
            vars : _.map( r.mergeVars, ( v, k ) => ({
                name    : k,
                content : v
            }) )
        }) )
    };
};

export default ({

    templateName,                                               // Required. Takes mandrill template's slug
    templateContent  = [],                                      // This is not to do w. merge vars, it's mandrill section templating
    
    // all used to modify base "message" cfg
    senderKey        = emailConstants.senders.edward.key,
    subject          = "Re: Cheylesmore Exchange",
    recipients       = [],                                      // takes [ { email, name, mergeVars } ]
    message          = {},                                      // modify additional keys (if needed)

    // mandrill's required sendTemplate : function cfg
    async            = false,
    ip_pool          = "Main pool",
    send_at          = new Date()

}) => {

    // add to base "message" cfg
    let messageCfg = _.assignIn( 
        baseCfg, 
        {
            subject,
            ... message,
            ... generateSenderCfg( senderKey ),
            ... generateRecipientCfg( recipients ),
            ... generateMergeVarsCfg( recipients )
        }
    );

    mandrill_client.messages.sendTemplate(
        {
            template_name       : templateName, 
            template_content    : templateContent,
            message             : messageCfg,

            async,
            ip_pool,
            send_at
        }, 
        result  => console.log( result ),
        err     => console.log( "A mandrill error occurred: " + err.name + " - " + err.message ) 
    );    
};