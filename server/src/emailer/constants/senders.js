export default {
    edward : {
        key   : "edward",
        name  : "Edward",
        email : "edward@cheylesmorehouse.co.uk"    
    },

    support : {
        key   : "support",
        name  : "Support",
        email : "support@cheylesmorehouse.co.uk"    
    }
};