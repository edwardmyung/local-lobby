import { default as globalMergeVars } from "./global-merge-vars";
import { default as senders }         from "./senders";

export default {
    globalMergeVars,
    senders
};