import emailConstants from "./constants";

export default {

    // Templating in variables (a.k.a. merging)
    merge             : true,
    merge_language    : "handlebars",
    global_merge_vars : emailConstants.globalMergeVars,

    // Generic config
    important    : false,         // prioritise in the mail queue
    track_opens  : true,          // mandrill analytics
    track_clicks : true,          // mandrill analytics
    inline_css   : true,          // turns <style> tags into inline css
    tags         : []             // [ "tags" ], used for mandrill analytics

};