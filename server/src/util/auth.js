import jwt    from "jsonwebtoken";
import cfg    from "../cfg";
import models from "../models";

export const requireAuth = async user => {
    /* 
    * Called in resolvers to test the user from the graphQL server's context
    */

    const msg = "Unauthorized - requireAuth blocking access";

    if ( !user || !user._id )
        throw new Error( msg );

    const myUser = await models.User.findById( user._id );

    if ( !myUser )
        throw new Error( msg );

    return myUser;
};

export const decodeToken = token => {
    var arr = token.split(" ");

    if ( arr[0] === "Bearer" ) {
        return jwt.verify( arr[1], cfg.constants.JWT_SECRET );
    }

    throw new Error( "Token not valid" );
};
