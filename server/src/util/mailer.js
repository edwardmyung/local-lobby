var mandrill = require( "mandrill-api/mandrill" );
var mandrill_client = new mandrill.Mandrill( process.env.MANDRILL_API_KEY );

var message = {
    "html": "<p>Sup bruh</p>",
    "text": "Test",
    "subject": "Test",
    "from_email": "edward@cheylesmorehouse.co.uk",
    "from_name": "Edward <edward@cheylesmorehouse.co.uk>",
    "to": [{
        "email": "edward.myung@googlemail.com",
        "name": "Edward Myung",
        "type": "to"
    }],
    "headers": {
        "Reply-To": "john@smtp.mandrillapp.com"
    },
    "important": false,
    "track_opens": null,
    "track_clicks": null,
    "auto_text": null,
    "auto_html": null,
    "inline_css": true,
    "url_strip_qs": null,
    "preserve_recipients": null,
    "view_content_link": null,
    "bcc_address": "john@smtp.mandrillapp.com",
    "tracking_domain": null,
    "signing_domain": null,
    "return_path_domain": null,
    "merge": true,
    "merge_language": "mailchimp",
    "tags": [
        "password-resets"
    ],
    "google_analytics_domains": [
        "example.com"
    ],
    "google_analytics_campaign": "message.from_email@example.com",
    "metadata": {
        "website": "www.example.com"
    },
    "recipient_metadata": [{
        "rcpt": "recipient.email@example.com",
        "values": {
            "user_id": 123456
        }
    }],
    "images": [{
        "type": "image/png",
        "name": "IMAGECID",
        "content": "ZXhhbXBsZSBmaWxl"
    }]
};

const async   = false;
const ip_pool = "Main Pool";
const send_at = new Date();

mandrill_client.messages.send({
    message, 
    async, 
    ip_pool, 
    send_at
}, function( result ) {
    console.log( result );
    /*
    [{
            "email": "recipient.email@example.com",
            "status": "sent",
            "reject_reason": "hard-bounce",
            "_id": "abc123abc123abc123abc123abc123"
        }]
    */
}, function(e) {
    // Mandrill returns the error as an object with name and message keys
    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
});