import emailer from "../../emailer";
import models from "../../models";

const namesFromFullName = fullName => {
    var nameArray = fullName.split( " " );

    return {
        firstName : nameArray[ 0 ],
        lastName  : nameArray.slice( 1 ).join( " " )
    };
};

export default {
    async login( _parent, { email, password }, _ctx ) {
        try {

            console.log( "======= 1 ========" );
            console.log( email, password );
            let user = await models.User.findOne({ email });

            if ( !user )
                throw new Error( "Email from attempted login does not exist" );
        
            if ( !user.isAuthenticated( password ) )
                throw new Error( "Password is incorrect" );
                
            return {
                token: user.createToken()
            };
        } catch ( err ) {
            throw err;
        }
    },

    async authenticateResidence( _parent, { houseNumber, shortId }, _ctx ) {
        /*
            * N.B. This auths House with code, NOT user with code
            * The user is intrinsically linked
            * 
            * This shortId is checked here and in register to:
            *   1. allow calling in two digsetible steps
            *   2. prevent client side bypassing 
        */
        try {
            let house = await models.House.findOne({ houseNumber });

            if ( shortId === house.authShortId )
                return { isCurrentResident : true };

            return { isCurrentResident: false };
        } catch ( err ) {
            throw err;
        }
    },

    async register( _parent, { fullName, email, mobile, password, shortId, houseNumber }, _ctx ) {
        try {
            let { firstName, lastName } = namesFromFullName( fullName );

            let house = await models.House.findOne({ houseNumber });

            if ( shortId !== house.authShortId )
                throw new Error( "Incorrect code for house" );

            let user = await models.User.create({
                firstName,
                lastName,
                email,
                mobile,
                password,
                houses: { current : house._id }
            });

            return { token : user.createToken() };
        } catch ( err ) {
            throw err;
        }
    },

    async recoverAccount( _parent, { email }, _ctx ) {
        try {
            let user = await models.User.findOne({ email });

            if( !user )
                throw new Error( "No account with that email exists" );

            emailer.sendTemplate({
                templateName : "recover-account",
                subject      : "Re: Recovering your account",
                senderKey    : "edward",
                recipients   : [
                    {
                        name : `${ user.firstName } ${ user.lastName }`,
                        email : user.email,
                        mergeVars : {
                            href : "https://www.messly.co.uk/@£$@£$@$£"
                        }
                    }
                ]
            });
                
            return { message : "Successfully sent recovery email" };
        } catch( err ) {
            throw err;
        }
    }
};
