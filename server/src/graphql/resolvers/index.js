import user         from "./user";
import exchangeItem from "./exchange-item";

export default {
    Mutation: {
        login                 : user.login,
        authenticateResidence : user.authenticateResidence,
        register              : user.register,
        recoverAccount        : user.recoverAccount,

        createExchangeItem    : exchangeItem.createExchangeItem,
        updateExchangeItem    : exchangeItem.updateExchangeItem,
        deleteExchangeItem    : exchangeItem.deleteExchangeItem,
    },

    Query: {
        getExchangeItems : exchangeItem.getExchangeItems
    }
};