import _      from "lodash";
import models from "../../models";
import util   from "../../util";

export default {
    getExchangeItems : async ( _parent, _args, _ctx ) => {
        try {
            return await models.ExchangeItem.find({});
        } catch ( err ) {
            throw err;
        }
    },

    createExchangeItem : async ( _parent, args, { user } ) => {
        try {
            await util.auth.requireAuth( user );

            return await models.ExchangeItem.create({ 
                ... args,
                seller : user._id
            });
        } catch ( err ) {
            throw err;
        }
    },

    updateExchangeItem : async ( _parent, { _id, ... rest }, { user } ) => {
        try {
            await util.auth.requireAuth( user );

            let item = await models.ExchangeItem.findOne({ 
                _id, 
                seller : user._id
            });

            if( !item )
                throw new Error( "Exchange item could not be found to update. Remember to check authed user owns object." );

            // update new args passed in
            _.map( rest, ( v, k ) => item[ k ] = v );

            return item.save();
        } catch ( err ) {
            throw err;
        }
    },

    deleteExchangeItem : async ( _parent, { _id }, { user } ) => {
        try {
            await util.auth.requireAuth( user );
            
            let item = await models.ExchangeItem.findOne({
                _id,
                user : user._id
            });

            if( !item )
                throw new Error( "No item with that id was found" );
            
            await item.remove();
            
            return { message : `Item ${ item.name } successfully deleted` };
        } catch( err ) {
            throw err;
        }
    }
};