import express          from "express";
import bodyParser       from "body-parser";
import cors             from "cors";
import constants        from "./cfg/constants";
import { ApolloServer } from "apollo-server-express";

import { readFileSync } from "fs";
const typeDefs = readFileSync( __dirname + "/graphql/schema.graphql", "utf8" );

import resolvers        from "./graphql/resolvers";
import playgroundTabs   from "./gql-playground-tabs";

// cfg imports require dotenv to make env vars accessible
import "./cfg/db";          
import cfg from "./cfg";

const app = express();
app.use( cors() );
app.use( bodyParser.json() );
app.use( cfg.middlewares.auth );

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }) => ({
        user: req.user
    }),
    playground: {
        settings: {
            "editor.cursorShape": "line",
            "editor.theme": "light"
        },
        tabs: playgroundTabs
    }
});

server.applyMiddleware({ app });

app.listen( constants.PORT , err => {
    if ( err ) {
        console.log( err );
    } else {
        console.log( `App is listening on port ${ constants.PORT }` );
    }
});