import _         from "lodash";
import faker     from "faker";

import models    from "../models";
import constants from "../constants";
import "../cfg";
import "../cfg/db";

const HOUSE_GROUP_COUNT = 1;
const HOUSE_COUNT = 10;

(async () => {
    try {
        await models.HouseGroup.deleteMany();
        await models.House.deleteMany();
        await models.User.deleteMany();
        await models.ExchangeItem.deleteMany();

        // Create HouseGroups
        _.map( _.range( 0, HOUSE_GROUP_COUNT ), async ( _x, ix ) => {
            var houseGroup = await models.HouseGroup.create({
                name: `HouseGroup ${ ix }`,
                location: {
                    coordinates: [
                        faker.address.latitude(),
                        faker.address.longitude()
                    ],
                    address: {
                        line1: faker.address.streetAddress(),
                        town: faker.address.county(),
                        postcode: faker.address.zipCode()
                    }
                }
            });

            // Create Houses
            _.map( _.range( 0, HOUSE_COUNT ), async ( _x, ix ) => {
                var house = await models.House.create({
                    houseNumber: ix,
                    houseGroup: houseGroup._id,
                    authShortId: "ABCDE"
                });

                // Create User for each House 0 - 4
                _.map( _.range( 0, 2 ), async _x => {
                    var user = await models.User.create({
                        firstName: faker.name.firstName(),
                        lastName: faker.name.lastName(),
                        email: faker.internet.email(),
                        mobile: faker.phone.phoneNumber(),
                        password: "foobar123",
                        houses: {
                            current: house._id,
                            historic: []
                        }
                    });

                    // Create ExchangeItems per User
                    _.map( _.range( 0, 6 ), async _x => {
                        await models.ExchangeItem.create({
                            name: faker.lorem.sentence(),
                            description: faker.lorem.sentences(),
                            condition: constants.exchangeItemCondition.new.key,
                            askingPrice: 0,
                            seller: user._id
                        });
                    });
                });
            });
        });
    } catch ( err ) {
        throw err;
    }
})();
