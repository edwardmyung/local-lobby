export default {
    new : {
        key  : "new",
        name : "New"
    },

    excellent : {
        key  : "excellent",
        name : "Excellent"
    },

    good : {
        key  : "good",
        name : "Good"
    },

    used : {
        key  : "used",
        name : "Used"
    },

    poor : {
        key  : "poor",
        name : "Poor"
    }
};