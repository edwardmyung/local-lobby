import mongoose, { Schema } from "mongoose";

const HouseGroupSchema = new Schema({
    name : {
        type     : String,
        required : "HouseGroup must have a name"
    },

    location : {
        coordinates : [
            {
                type     : Number,
                required : "You must provide coordinates"
            }
        ],

        address : {
            line1 : {
                type     : String,
                required : "Address line 1 is required"
            },

            line2 : {
                type : String
            },

            town : {
                type     : String,
                required : "Town is required"
            },

            postcode : {
                type     : String,
                required : "Postcode is required"
            }
        }
    }
});

export default mongoose.model( "HouseGroup", HouseGroupSchema );