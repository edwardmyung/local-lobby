import mongoose, { Schema } from "mongoose";

const ExchangeItemSchema = new Schema(
    {
        name : {
            type      : String,
            minLength : [ 3, "You must give your item a name." ],
            maxLength : [ 140, "This item's name is too long." ],
            trim      : true
        },

        description : {
            type      : String,
            maxLength : [ 200 ],
            trim      : true
        },

        condition : {
            type     : String,
            required : "You must select a condition"
        },

        seller : {
            type     : Schema.Types.ObjectId,
            ref      : "User",
            required : "Exchange item must be owned by a user"
        },

        askingPrice: {
            type     : Number,
            required : "You must set a price. This price can be 0."
        },

        endDt: {
            type : Schema.Types.Date
        }
    },
    { timestamps : true }
);

export default mongoose.model( "ExchangeItem", ExchangeItemSchema );