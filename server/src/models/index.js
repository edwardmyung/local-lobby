import { default as HouseGroup }   from "./house-group";
import { default as House }        from "./house";
import { default as User }         from "./user";
import { default as ExchangeItem } from "./exchange-item";

export default {
    HouseGroup,
    House,
    User,
    ExchangeItem
};