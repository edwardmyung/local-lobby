import mongoose, { Schema } from "mongoose";
import { hashSync, compareSync } from "bcrypt-nodejs";
import jwt from "jsonwebtoken";
import cfg from "../cfg";

const UserSchema = new Schema(
    {
        password : {
            type     : String,
            required : "User requires a password"
        },

        firstName : {
            type     : String,
            required : "First name is required"
        },

        lastName : {
            type     : String,
            required : "Last name is required"
        },

        email : {
            type     : String,
            required : "Email is required",
            unique   : "User email must be unique"
        },

        mobile : {
            type     : "String",
            required : "A mobile number is required",
            unique   : "User mobile must be unique"
        },

        houses : {
            current : {
                type : Schema.Types.ObjectId,
                ref  : "House"
            },

            historic : [
                {
                    type : Schema.Types.ObjectId,
                    ref  : "House"
                }
            ]
        }
    },
    { timestamps : true }
);

UserSchema.pre( "save", function( next ) {
    if ( this.isModified( "password" ) ) {
        this.password = this._hashPassword( this.password );
        next();
    }

    next();
});

UserSchema.methods = {
    _hashPassword( password ) {
        return hashSync( password );
    },

    isAuthenticated(password) {
        return compareSync( password, this.password );
    },

    createToken() {
        return jwt.sign( { _id : this._id }, cfg.constants.JWT_SECRET );
    }
};

export default mongoose.model( "User", UserSchema );
