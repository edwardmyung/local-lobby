import mongoose, { Schema } from "mongoose";

const HouseSchema = new Schema(
    {
        houseNumber : {
            type     : Number,
            required : "House must have a number."
        },

        houseGroup : {
            type : Schema.Types.ObjectId,
            ref  : "HouseGroup"
        },

        authShortId : {
            type     : String,
            required : "Each house must have an authentication shortid"
        }
    },
    { timestamps : true }
);

module.exports = mongoose.model( "House", HouseSchema );